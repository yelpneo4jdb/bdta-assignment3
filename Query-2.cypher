// Query (2)

// How many are there unique restaurant categories?
// match (b:Business)-[:IS_OF_CATEGORY]->(c:Category{name:'Restaurants'})
// match (b)-[:IS_OF_CATEGORY]->(m) 
// return count(DISTINCT m.name)

// Assuming all businesses are restaurants
match (c:Category)
return count(distinct (c.name)) as UniqueCategories
