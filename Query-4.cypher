// Query (4)

//How many people with more than 1000 friends?

// Version 1
//match p = (u:User) - [:HAS_FRIEND]->()
//with u, count(p) as total_friends
//where total_friends > 1000
//return count(u)

// Version 2
match (u:User)
where size((u)-[:HAS_FRIEND]->()) > 1000
return u.name

// Version 3 (optimized)
match (u:User)
call apoc.neighbors.athop.count(u, "HAS_FRIEND",1)
YIELD value
where value > 1000
return count(u)

