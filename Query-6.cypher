// Query (6)

// Get the list of people whose friends last time wrote a review for a Chinese restaurant

// Version 1 (4 matches)
//match (b:Business)-[:IS_OF_CATEGORY]->(c:Category) 
//where toLower(c.name) contains 'chinese'
//match (r:Review)-[:REVIEW_FOR]->(b)
//with max(apoc.date.parse(r.date)) as maximum
//match (r:Review)<-[:WROTE_REVIEW]-(friend:User)
//where apoc.date.parse(r.date)=maximum
//match (u:User)-[:HAS_FRIEND]->(friend)
//return u.name


// Version 2 (2 matches)
//match (r:Review)-[:REVIEW_FOR]->(:Business)-[:IS_OF_CATEGORY]->(c:Category)

//where toLower(c.name) contains 'chinese'
//with max(apoc.date.parse(r.date)) as last

//match (u:User)-[:HAS_FRIEND]->(:User)-[:WROTE_REVIEW]->(r2:Review)

//where apoc.date.parse(r2.date) = last
//return u.name


// Version 3 (2 matches, assuming 'Chinese' appears in business name)
match (r:Review)-[:REVIEW_FOR]->(:Business)
where toLower(b.name) contains 'chinese'
with max(apoc.date.parse(r.date)) as last
match (u:User)-[:HAS_FRIEND]->
(:User)-[:WROTE_REVIEW]->(r2:Review)
where apoc.date.parse(r2.date) = last
return u.name
