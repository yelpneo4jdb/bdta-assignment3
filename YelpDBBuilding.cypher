// Graph building steps

# Create the DB using neo4j-admin import tool
sudo neo4j-admin import \
--delimiter="\t" \
--database=yelp.db \
--nodes=User=all_users_header.tsv,all_users.tsv \
--nodes=Business=business_header.tsv,business.tsv \
--nodes=Category=categories_header.tsv,categories.tsv \
--nodes=Review=reviews_header.tsv,reviews.tsv \
--relationships=HAS_FRIEND=has_friend_header.tsv,has_friend.tsv \
--relationships=IS_OF_CATEGORY=is_of_category_header.tsv,is_of_category.tsv \
--relationships=REVIEW_FOR=review_for_header.tsv,review_for.tsv \
--relationships=WROTE_REVIEW=wrote_review_header.tsv,wrote_review.tsv \


// STEP 1 - Creating Business nodes
CALL apoc.periodic.iterate(' 
    LOAD CSV with headers FROM "file:///business.tsv" as row FIELDTERMINATOR "\t" return row
    ','
    
    CREATE (b:Business{name: row.name, address:row.address, city: row.city, state:row.state, business_id:row.business_id})
    
    ',
    {batchSize:6000}
)


// STEP 2 - Creating Index on :Business(business_id) and :Category(name)

CREATE INDEX ON :Business(business_id)
CREATE INDEX ON :Category(name)


// STEP 3 - Creating Category node with IS_OF_CATEGORY relationship

CALL apoc.periodic.iterate(' 
    LOAD CSV with headers FROM "file:///business.tsv" as row FIELDTERMINATOR "\t" return row
    ','
    UNWIND apoc.text.split(row.categories, ",") as category
    WITH trim(category) AS category, row AS row
    
    MATCH (b:Business{business_id:row.business_id})
    merge (c:Category{name:category})
    CREATE (b)-[:IS_OF_CATEGORY]->(c)
    ',
    {batchSize:500}
1)


// users data
// Step 4 - Creating Users
:auto
USING PERIODIC COMMIT 6000
LOAD CSV with headers FROM "file:///users.tsv" as row FIELDTERMINATOR "\t"
create (:User{user_id:row.user_id})



// Step 5 - Creating Index
CREATE INDEX ON :User(user_id)




// Step 6 - Creating HAS_FRIEND relationships
CALL apoc.periodic.iterate(' 
    LOAD CSV with headers FROM "file:///users.tsv" as row FIELDTERMINATOR "\t" return row
    ','
    UNWIND apoc.text.split(row.friends, ",") as friend
    WITH trim(friend) AS friend, row.user_id AS id
    
    MATCH (u1:User{user_id:id})
    MERGE (u2:User{user_id:friend})
    CREATE (u1)-[:HAS_FRIEND]->(u2)
    ',
    {batchSize:500}
)

// Step 7 - Setting the name of each user
CALL apoc.periodic.iterate(' 
    LOAD CSV with headers FROM "file:///users.tsv" as row FIELDTERMINATOR "\t" return row
    ','
    
    MATCH (u1:User{user_id:row.user_id})
    SET u1.name = row.name
    
    ',
    {batchSize:6000}
)


// Step 8 - Creating Review nodes

CALL apoc.periodic.iterate(' 
    LOAD CSV with headers FROM "file:///reviews.tsv" as row FIELDTERMINATOR "\t" return row
    ','

    Create (:Review{review_id:row.review_id, date: row.date})
    
    ',
    {batchSize:6000}
)



// Step 9 - Creating Index on Review(review_id)
CREATE INDEX ON :Review(review_id)



// Step 10 - Creating WROTE_REVIEW and REVIEW_FOR relationships
CALL apoc.periodic.iterate(' 
    LOAD CSV with headers FROM "file:///reviews.tsv" as row FIELDTERMINATOR "\t" return row
    ','
    
    MATCH (r:Review{review_id:row.review_id})
    MATCH (u:User{user_id:row.user_id})
    MATCH (b:Business{business_id:row.business_id})
    MERGE (u)-[:WROTE_REVIEW]->(r)-[:REVIEW_FOR]->(b)
    
    ',
    {batchSize:600}
)


