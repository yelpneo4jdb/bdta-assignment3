// query 1
// Which state has more businesses registered on yelp?
match (b:Business)
return b.state as state , count(b.state) as count
order by count desc
limit 1