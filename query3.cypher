// query 3
// Get distribution of restaurant categories in the city Phoenix, sort by count in descending order, 
// show the top 10
match (b:Business)-[:IS_OF_CATEGORY]->
(c:Category{name:'Restaurants'})
where toLower(b.city) contains 'phoenix' 
match ((b)-[:IS_OF_CATEGORY]->(m:Category))
with m, count(m) as categories_count
return m.name, categories_count
order by categories_count desc
limit 10