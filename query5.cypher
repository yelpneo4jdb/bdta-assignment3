// query 5
// Get restaurants that have their ratings decreased from 2014 to 2017
match (r:Review)-[:REVIEW_FOR]->(b:Business)
where apoc.date.parse("2014-01-01 00:00:00") < apoc.date.parse(r.date) < apoc.date.parse("2014-12-31 23:59:59")
match (s:Review)-[:REVIEW_FOR]->(b)-[:IS_OF_CATEGORY]->(c) 
where apoc.date.parse("2017-01-01 00:00:00") < apoc.date.parse(s.date) < apoc.date.parse("2017-12-31 23:59:59")
with b, (avg(s.score)-avg(r.score)) as exp 
where exp < 0
return b.name
