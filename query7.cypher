// query 7
// For users with 5 to 10 friends, recommend new friends from the list of friends of their friends. 
// Rank candidates according to how well they are connected with initial users
match (u:User)-[:HAS_FRIEND]->(friends:User)
with u, count(friends) as total
where ((total >=5) and (total <=10)) 
match (u:User)-[:HAS_FRIEND*2]->(friendsfriends:User)
where (u.user_id <> friendsfriends.user_id)
return u.name, friendsfriends.name, size((u)-[*]->(friendsfriends))
order by size((u)-[*]->(friendsfriends)) desc
